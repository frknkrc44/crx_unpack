SHELL=/bin/bash
PYTHON=python3
PKG_NAME=crx_unpack

default: | clean check_tags bundle register upload
	@echo "Full service complete"

clean:
	@echo "Removing the build/ dist/ and *.egg-info/ directories"
	@rm -rf build dist *.egg-info

check_tags:
	@VER=`cat ${PKG_NAME}/VERSION`; \
	echo "Making sure that a tag has been created with the correct version number ($$VER)"; \
	TAGS=`git tag -l $$VER`; \
	if echo $$TAGS | grep -q $$VER; then echo "Found tag for version $$VER"; \
	else echo "No git tag '"$$VER"' found. You can create it with the following command:"; \
	echo; echo "git tag $$VER && git push --tags origin"; echo; exit 1; fi

bundle:
	@echo "Bundling the code"; echo
	@${PYTHON} setup.py sdist bdist_wheel

register:
	@echo; echo "Registering this version of the package on PyPI"; echo
	@-for bundle in dist/*; do ${PYTHON} `which twine` register $$bundle; done

upload:
	@echo "Uploading built package to PyPI"
	@${PYTHON} `which twine` upload dist/*

test: | clean check_tags bundle register_test upload_test
	@echo; echo "Full test complete. Package available on Test PyPI."

register_test:
	@echo; echo "Registering this version of the package on Test PyPI"; echo
	@-for bundle in dist/*; do ${PYTHON} `which twine` register $$bundle -r testpypi; done

upload_test:
	@echo; echo "Uploading built package to Test PyPI"
	@${PYTHON} `which twine` upload dist/* -r testpypi
